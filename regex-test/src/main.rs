extern crate regex;
extern crate clap;
extern crate ansi_term;

use std::fs::File;
use std::io::BufRead;
use std::path::Path;

use regex::Regex;

use clap::{Arg, App, SubCommand};

use ansi_term::Colour;
use ansi_term::Style;

fn main() {
    println!("= Simple file regex test =");

    let arg_matches = App::new("CLI Test")
            .version("0.1")
            .author("Jeff D")
            .about("A test of CLI stuff.")
            .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("input_file")
                .help("The file to process")
                .takes_value(true)
                )
            .arg(Arg::with_name("pattern")
                .short("r")
                .long("regex")
                .value_name("regex")
                .help("The pattern to match per line of the file.")
                .takes_value(true)
                )
            .get_matches();
    
    let filename = arg_matches.value_of("file").unwrap_or("default.txt");
    let pattern = arg_matches.value_of("pattern").unwrap_or(".*");
    let regex = Regex::new(pattern).unwrap();

    let path = Path::new(filename);
    let file = match File::open(&path) {
        Err(why) => 
            panic!("Not able to open file {}: {}", 
                    path.display(), why),
        Ok(file) => file,
    };

    let line_reader = std::io::BufReader::new(file);
    let mut count = 0;

    let match_style = Colour::Blue.bold();

    for line in line_reader.lines() {
        if let Ok(l) = line {
            match regex.find(&l) {
                Some(m) => {
                    let start = &l[0..m.start()];
                    let match_str = &l[m.start()..m.end()];
                    let end = &l[m.end()..l.len()];
                    println!("{}: {}{}{}", count, start, match_style.paint(match_str), end);
                },
                None => ()
            }
        }
        count = count + 1;
    }
}
