
trait Shape { 
    fn area(&self) -> f32;
}

struct Square {
    length: f32
}

struct Circle {
    radius: f32
}

impl Shape for Square {
    fn area(&self) -> f32 {
        return self.length*self.length;
    }
}

impl Shape for Circle {
    fn area(&self) -> f32 {
        return std::f32::consts::PI * self.radius * self.radius;
    }
}

fn main() {
    let sq = Square { length: 10.0  };
    let cr = Circle { radius: 10.0  };

    println!("Traits: Circle area: {}, square area: {}", cr.area(), sq.area());

    let mut shapes : Vec<Box<dyn Shape>> = Vec::new();
    shapes.push(Box::new(Square { length: 5.0 }));
    shapes.push(Box::new(Circle { radius: 1.0 }));

    shapes.iter().enumerate().for_each(|(idx, s)| {
        println!("Shape {}: Area={}", idx, s.area());
    })
}
