use std::io;
use std::io::prelude::*;

use sfml::window::{Window, Event, Style};
use sfml::graphics::{RenderWindow, Image, Texture, Sprite, Transformable, Drawable, RenderTarget, Color};
use sfml::system::{Vector2f};

use palette::{Rgb, RgbHue, Hsv};

extern crate num;
use num::{abs};
use num::complex::{Complex};

const MAX_ITER : u8 = 100;
const RE_START : f32 = -2.0;
const RE_END : f32 = 1.0;
const IM_START :f32 = -1.0;
const IM_END : f32 = 1.0;

const WIDTH : u32 = 800;
const HEIGHT : u32 = 600;

fn mandelbrot(c : Complex::<f32>) -> u8 {
    let mut z = Complex::new(0.0, 0.0);
    let mut n : u8 = 0;
    while abs(z.norm()) < 2.0 && n < MAX_ITER {
        z = z*z + c;
        n += 1;
    }

    return n;
}

fn main() {

    // Create a new window
    let mut window = RenderWindow::new((WIDTH, HEIGHT),
                                "SFML test",
                                Style::CLOSE,
                                &Default::default());

    // Limit the framerate to 60 frames per second (this step is optional)
    window.set_framerate_limit(60);

    let mut img = Image::new(WIDTH, HEIGHT);

    // Draw the Mandelbrot fractal
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            let c = Complex::new(
                    RE_START + (x as f32/WIDTH as f32) * (RE_END-RE_START), 
                    IM_START + (y as f32/HEIGHT as f32) * (IM_END-IM_START)
                );

            let num_iter = mandelbrot(c);

            let hue = RgbHue::from_radians(num_iter as f32 / MAX_ITER as f32); 
            let saturation = 1.0;
            let value = if num_iter < MAX_ITER {
                1.0
            }
            else {
                0.0
            };

            let rgb : Rgb = Hsv::new(hue, saturation, value).into();
            //let rgb = Rgb::from_hsv(hsv);

            let col = Color::rgb(
                (rgb.red*255.0) as u8,
                (rgb.green*255.0) as u8,
                (rgb.blue*255.0) as u8
            );

            img.set_pixel(x, y, col);
        }

        print!(".");
        io::stdout().flush().unwrap();
    }

    println!("");

    let t = Texture::from_image(&img).unwrap();
    let s = Sprite::with_texture(&t);

    // The main loop - ends as soon as the window is closed
    while window.is_open() {
        // Event processing
        while let Some(event) = window.poll_event() {
            // Request closing for the window
            if event == Event::Closed {
                window.close();
            }
        }

        // Activate the window for OpenGL rendering
        window.set_active(true);

        window.draw(&s);

        // End the current frame and display its contents on screen
        window.display();
    }
}