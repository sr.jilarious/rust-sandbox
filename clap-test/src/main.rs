use clap;

fn main() {
    let app_matches = clap::App::new("Clap test prog")
        .version("1.0")
        .author("Jeff DeWall")
        .about("Super awesome test program")
        .subcommand(
            clap::App::new("post")
                .arg(clap::Arg::with_name("url"))
            )
        .subcommand(
            clap::App::new("get")
                .arg(clap::Arg::with_name("url"))
        )
        .get_matches();
    
    match app_matches.subcommand() {
        ("get", Some(sub_matches)) => { println!("Get action called.") }
        ("post", Some(sub_matches)) => { 
            println!("Post action called.");
            match sub_matches.value_of("url") {
                Some(val) => {println!("    - pos arg: {}", val) }
                None => ()
            }
        }
        (_, _) => { println!("Unknown command!")}
    }
}
