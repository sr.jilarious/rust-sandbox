use serde::{Serialize, Deserialize};

#[derive(Serialize, Debug)]
pub enum TodoPriority
{
    Critical,
    High,
    Normal,
    Low,
    Background
}

#[derive(Serialize, Debug)]
pub struct TodoItem
{
    pub id: i32,
    pub name: String,
    pub description: String,
    pub priority: TodoPriority
}

#[derive(Serialize, Debug)]
pub struct TodoList
{
    pub name: String,
    pub items: Vec<TodoItem>
}
