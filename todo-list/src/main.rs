extern crate clap;
use clap::{Arg, App, SubCommand};

mod todo;
use todo::{TodoList, TodoItem, TodoPriority};

fn main() {
    
    let list1 = TodoList {
        name: String::from("Groceries"),
        items: vec![
            TodoItem {
                id: 0,
                name: String::from("Milk"),
                description: String::from(""),
                priority: TodoPriority::Normal
            },
            TodoItem {
                id: 1,
                name: String::from("Soda"),
                description: String::from(""),
                priority: TodoPriority::High
            },
        ]
    };

    let item1 = TodoItem {
        id: 0,
        name: String::from("Take out trash"),
        description: String::from("Put trash in bin, recycling too!"),
        priority: TodoPriority::Normal
    };

    let matches = App::new("todo!")
            .version("1.0")
            .author("Jeff D. <srjil@fastmail.net")
            .about("Amazing todo list application!")
            .subcommand(SubCommand::with_name("list")
                    .about("controls testing features"))
            .subcommand(SubCommand::with_name("add")
                    .about("controls testing features"))
            .subcommand(SubCommand::with_name("remove")
                    .about("controls testing features"))
            .get_matches();

    if let Some(_matches) = matches.subcommand_matches("list") {
        println!("list called!")
    }
    else if let Some(_matches) = matches.subcommand_matches("add") {
        println!("add called!")
    }
    else if let Some(_matches) = matches.subcommand_matches("remove") {
        println!("remove called!")
    }

    println!("Item has '{0}' - '{1}', {2:?}", item1.name, item1.description, item1.priority);
    
    println!("List: {}", list1.name);
    for it in &list1.items {
        println!("'{0}' - '{1}', {2:?}", it.name, it.description, it.priority);
    }

    let serialized = serde_json::to_string(&list1).unwrap();
    println!("Serialized: {}", serialized);
}
