mod mylib;
use mylib::MyLib;

fn main() {
    let obj : mylib::MyLibData = MyLib::new(32, 42);
    println!("Some things we should not know!");
    obj.cool_stuff();
}
