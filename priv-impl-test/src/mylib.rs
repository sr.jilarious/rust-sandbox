pub struct MyLibData
{
    pub pub_data : u32,
    priv_data : u32
}

pub trait MyLib
{
    fn new(value :u32, priv_value: u32) -> Self;
    fn cool_stuff(&self);
}


fn secret_stuff(obj : &MyLibData) {
    println!("This is secret sauce! Got priv data: {0}", obj.priv_data);
}

pub(crate) mod private {
    pub trait PrivMyLib
    {
        fn secret_stuff2(&self);
    }

    impl PrivMyLib for crate::mylib::MyLibData {
        fn secret_stuff2(&self) {
            println!("This is more secret sauce. {0}, {1}", 
                    self.pub_data, self.priv_data);
        }
    }
}

use crate::mylib::private::PrivMyLib;

impl MyLib for MyLibData {
    fn new(value :u32, priv_value: u32) -> Self
    {
        return MyLibData{pub_data: value, priv_data: priv_value}
    }

    fn cool_stuff(&self)
    {
        println!("Got public data: {0}, and priv data: {1}", self.pub_data, self.priv_data);
        secret_stuff(self);
        self.secret_stuff2();
    }
}
