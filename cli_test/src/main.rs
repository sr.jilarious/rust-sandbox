
extern crate clap;
use clap::{Arg, App, SubCommand};

fn main() {
    let arg_matches = App::new("CLI Test")
            .version("0.1")
            .author("Jeff D")
            .about("A test of CLI stuff.")
            .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("input_file")
                .help("The file to process")
                .takes_value(true)
                )
            .get_matches();
    
    let filename = arg_matches.value_of("file").unwrap_or("default.txt");
    println!("Got an input file: '{}'", filename);
}
