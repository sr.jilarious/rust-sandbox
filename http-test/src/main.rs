#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let response = reqwest::get("https://www.walknsqualk.com")
        .await?;

    println!("Status: {}", response.status());
    println!("Headers:\n{:#?}", response.headers());
    println!("{:#?}", response.text().await?);

    Ok(())
}

// use reqwest;

// fn main() -> Result<(), Box<dyn std::error::Error>> {
//     let body = reqwest::blocking::get("https://www.rust-lang.org")?
//     .text()?;
//     println!("body = {:?}", body);
//     // let response = reqwest::blocking::get("https://www.walknsqualk.com");
    
//     // println!("Status: {}", response.status());
//     // println!("Headers:\n{:#?}", response.headers());

//     // let mut body = String::new();
//     // response.read_to_string(&mut body);
//     // println!("Body:\n{}", body);
//     Ok(())
// }
