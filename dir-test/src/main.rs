use std::io;
use std::fs::{self, DirEntry, Metadata};
use std::path::Path;
use std::env;

fn main() ->  Result<(), Box<dyn std::error::Error>> {
    let path = env::current_dir()?;

    for entry in fs::read_dir(path)? {
        let entry = entry?;
        let path = entry.path();
        let metadata = fs::metadata(&path)?;
        let entry_fn = entry.file_name();
        let file_name = entry_fn.into_string().unwrap();

        let suffix = if path.is_dir() { "/"} else {""};
        println!("{}{}", file_name, suffix);
    }

    Ok(())
}
