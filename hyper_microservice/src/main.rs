use std::convert::Infallible;
use std::net::SocketAddr;
use hyper::{Body, Request, Response, Server};
use hyper::service::{make_service_fn, service_fn};
use tokio;

async fn handle(_req: Request<Body>) -> Result<Response<Body>, Infallible> {
    Ok(Response::new(Body::from("First rust microservice!")))
}

#[tokio::main]
async fn main() {
    println!("== Hyper microservice ==");

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    
    let make_svc = make_service_fn(|_conn| async {
        // service_fn converts our function into a `Service`
        Ok::<_, Infallible>(service_fn(handle))
    });
    
    let server = Server::bind(&addr).serve(make_svc);
    // And run forever...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}
