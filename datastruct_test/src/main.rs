use std::collections::{LinkedList, HashMap};

fn print_vec( v : &mut Vec<i32>)
{
    println!("My fancy vec is: {:?}", v);
    v[0] = 5;
}

fn main() {
    let mut v1 = vec![1i32, 2, 3, 4, 5];
    print_vec(&mut v1);
    println!("My vector is: {:?}", v1);

    // Test out std linked lists.
    let mut ll : LinkedList<u32> = LinkedList::new();
    assert!(ll.is_empty());
    ll.push_back(10);
    ll.push_back(20);
    assert!(ll.len() == 2);

    ll.extend([30, 40, 50, 60].iter());
    println!("Linked list contains: ");
    for x in &ll {
        print!("{}, ", x);
    }
    println!("");

    // Try out hash map
    let mut hm : HashMap<String, u32> = HashMap::new();
    hm.insert("Item 1".to_string(), 100);
    hm.insert("Game".to_string(), 5);
    hm.insert("Book".to_string(), 123);
    match hm.get("Game") {
        Some(n) => {
            println!("Game: {}", n);
        },
        None => ()
    }

    match hm.get("Non-existant") {
        Some(n) => println!("No expected!"),
        None => println!("Non-existant is not there as expected.")
    }
}
