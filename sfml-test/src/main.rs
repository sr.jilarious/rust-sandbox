use sfml::window::{Window, Event, Style};
use sfml::graphics::{RenderWindow, Texture, Sprite, Transformable, Drawable, RenderTarget, Color};
use sfml::system::{Vector2f};

extern crate rand;

use rand::Rng;

struct Ball<'lifetime> {
    sprite: Sprite<'lifetime>,
    pos: Vector2f, 
    vel: Vector2f,
}


fn main() {

    let mut rng = rand::thread_rng();

    let textures = vec!(
            Texture::from_file("assets/blue_piece.png"),
            Texture::from_file("assets/green_piece.png"),
            Texture::from_file("assets/red_piece.png")
        );

    let mut balls = Vec::<Ball>::new();
    for _ in 0..50 {
        let tex = textures[rng.gen_range(0, 3)].as_ref().unwrap();
        let mut ball = Ball { 
                sprite: Sprite::with_texture(&tex),
                pos: Vector2f {x: rng.gen_range(100.0, 700.0), y: rng.gen_range(100.0, 500.0)},
                vel: Vector2f {x: rng.gen_range(-5.0, 5.0), y: rng.gen_range(-5.0, 5.0)},
            };
        balls.push(ball)
    }

    // Create a new window
    let mut window = RenderWindow::new((800, 600),
                                "SFML test",
                                Style::CLOSE,
                                &Default::default());

    // Limit the framerate to 60 frames per second (this step is optional)
    window.set_framerate_limit(60);

    // The main loop - ends as soon as the window is closed
    while window.is_open() {
        // Event processing
        while let Some(event) = window.poll_event() {
            // Request closing for the window
            if event == Event::Closed {
                window.close();
            }
        }

        // Update all of the balls.
        for mut b in &mut balls {
            if (b.pos.x + b.vel.x > window.size().x as f32) || (b.pos.x + b.vel.x < 0.0){
                b.vel.x = -b.vel.x;
            }

            if (b.pos.y + b.vel.y > window.size().y as f32) || (b.pos.y + b.vel.y < 0.0){
                b.vel.y = -b.vel.y;
            }
            
            b.pos = b.pos + b.vel;
            b.sprite.set_position(b.pos);
        }

        // Draw everything
        window.clear(Color::rgb(20, 20, 48));

        for b in &balls {
            window.draw(&b.sprite);
        }

        // Activate the window for OpenGL rendering
        window.set_active(true);

        // End the current frame and display its contents on screen
        window.display();
    }
}