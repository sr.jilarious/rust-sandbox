# Future project ideas

Here is a list of some simple-ish projects I could do here:

- license key generator
- multithreaded download: download in one thread, progress bar in cli
- dual pane file manager
- command line tools:
    - ls with json output
        - add visualization module
    - ps with json output
        - add visualization
    