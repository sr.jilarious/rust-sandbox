
use actix_web::{
    get, post, error, middleware, web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder
};

use serde::{Deserialize, Serialize};

#[get("/{id}/{name}/index.html")]
async fn index(web::Path((id, name)): web::Path<(u32, String)>) -> impl Responder {
    return format!("Hello {}! id:{}", name, id)
}

#[derive(Deserialize)]
struct User {
    name : String,
}

async fn name(info: web::Json<User>) -> std::io::Result<String>
{
    Ok(format!("Hi there {}", info.name))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            // enable logger
            .wrap(middleware::Logger::default())
            .data(web::JsonConfig::default().limit(4096)) // <- limit size of the payload (global configuration)

            // .service(web::resource("/extractor").route(web::post().to(index)))
            // .service(
            //     web::resource("/extractor2")
            //         .data(web::JsonConfig::default().limit(1024)) // <- limit size of the payload (resource level)
            //         .route(web::post().to(extract_item)),
            // )
            // .service(web::resource("/manual").route(web::post().to(index_manual)))
            // .service(web::resource("/mjsonrust").route(web::post().to(index_mjsonrust)))
            .service(index)
            .route("/name", web::post().to(name))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}