//! Blinks the LED on a Pico board
//!
//! This will blink an LED attached to GP25, which is the pin the Pico uses for the on-board LED.
#![no_std]
#![no_main]

use core::fmt::Write;
use cortex_m_rt::entry;
use embedded_hal::digital::v2::InputPin;
use embedded_hal::digital::v2::OutputPin;
use embedded_time::rate::*;
use panic_halt as _;
use pico::{
    hal::{
        clocks::{init_clocks_and_plls, Clock},
        gpio,
        pac,
        sio::Sio,
        watchdog::Watchdog,
        uart::UartPeripheral,
    },
    Pins, XOSC_CRYSTAL_FREQ,
};
#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER;

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    let mut watchdog = Watchdog::new(pac.WATCHDOG);

    let clocks = init_clocks_and_plls(
        XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());

    let sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut uart = UartPeripheral::<_, _>::enable(
        pac.UART0,
        &mut pac.RESETS,
        pico::hal::uart::common_configs::_9600_8_N_1,
        clocks.peripheral_clock.into(),
    )
    .unwrap();

    let _tx_pin = pins.gpio0.into_mode::<gpio::FunctionUart>();
    let _rx_pin = pins.gpio1.into_mode::<gpio::FunctionUart>();

    let mut led_pin = pins.led.into_push_pull_output();
    //let mut led2_pin = pins.gpio15.into_push_pull_output();
    
    let button_pin = pins.gpio15.into_pull_up_input();

    let mut tick = false;
    let mut debounce = false;

    loop {
        if button_pin.is_high().unwrap() {
            led_pin.set_low().unwrap();
            debounce = false;
            if tick {
                tick = false;
                write!(uart, "-").unwrap();
            }
            else {
                tick = true;
                write!(uart, ".").unwrap();
            }
        }
        else {
            led_pin.set_high().unwrap();
            if debounce == false {
                debounce = true;
                uart.write_full_blocking(b"UART example\r\n");
            }
        }

        delay.delay_ms(100);       
    }
}
