//! Sends some commands to an LCD panel hooked up over i2c

#![no_std]
#![no_main]
#![allow(dead_code)]

use core::fmt::Write;
use cortex_m_rt::entry;
use embedded_hal::digital::v2::InputPin;
use embedded_hal::digital::v2::OutputPin;
use embedded_time::rate::*;
use panic_halt as _;
use pico::{
    hal::{
        clocks::{init_clocks_and_plls, Clock},
        gpio,
        pac,
        sio::Sio,
        watchdog::Watchdog,
        uart::UartPeripheral,
        gpio::FunctionI2C,
        i2c::I2C,
    },
    Pins, XOSC_CRYSTAL_FREQ,
};

mod lcd_display;
use crate::lcd_display::LcdDisplay;

#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER;

const SYS_HZ: u32 = 125_000_000_u32;


#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    let mut watchdog = Watchdog::new(pac.WATCHDOG);

    let clocks = init_clocks_and_plls(
        XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());

    let sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut uart = UartPeripheral::<_, _>::enable(
        pac.UART0,
        &mut pac.RESETS,
        pico::hal::uart::common_configs::_9600_8_N_1,
        clocks.peripheral_clock.into(),
    )
    .unwrap();

    let _tx_pin = pins.gpio0.into_mode::<gpio::FunctionUart>();
    let _rx_pin = pins.gpio1.into_mode::<gpio::FunctionUart>();

    let mut led_pin = pins.led.into_push_pull_output();
    //let mut led2_pin = pins.gpio15.into_push_pull_output();
    
    let button_pin = pins.gpio15.into_pull_up_input();

    let sda_pin = pins.gpio18.into_mode::<FunctionI2C>();
    let scl_pin = pins.gpio19.into_mode::<FunctionI2C>();

    let i2c = I2C::i2c1(
        pac.I2C1,
        sda_pin,
        scl_pin,
        400.kHz(),
        &mut pac.RESETS,
        SYS_HZ.Hz(),
    );

    let mut lcd = lcd_display::LcdDisplayDriver{i2c: i2c, delay: delay};

    led_pin.set_high().unwrap();
    
    
    lcd.init();

    let lines = [
        "Hello", "World",
        "How are you", "doing today?",
        "Written by", "Sr.Jil 2021",
        ];
    let mut idx = 0;
    lcd.draw_lines(lines[0], lines[1]);

    let mut tick = false;
    let mut debounce = false;
        
    loop {
        if button_pin.is_low().unwrap() {
            led_pin.set_high().unwrap();
            if !debounce {
                debounce = true;

                let l1 = lines[idx*2];
                let l2 = lines[idx*2 + 1];

                write!(uart, "Setting LCD message to '{}', '{}'.\r\n", l1, l2).unwrap();
                lcd.draw_lines(l1, l2);

                idx += 1;
                if idx >= 3 {
                    idx = 0;
                }
            }
        }
        else {
            debounce = false;
            if tick {
                tick = false;
                //write!(uart, "-").unwrap();
                led_pin.set_low().unwrap();
            }
            else {
                tick = true;
                led_pin.set_high().unwrap();
                //write!(uart, ".").unwrap();
            }
        // if debounce == false {
        //     debounce = true;
        // }
        }

        lcd.delay.delay_ms(100);       
    }
}
