
#![allow(dead_code)]

// use core::fmt::Write;
// use cortex_m_rt::entry;
// use embedded_hal::digital::v2::InputPin;
// use embedded_hal::digital::v2::OutputPin;
// use embedded_time::rate::*;
// use panic_halt as _;
// use pico::{
//     hal::{
//         clocks::{init_clocks_and_plls, Clock},
//         gpio,
//         pac,
//         sio::Sio,
//         watchdog::Watchdog,
//         uart::UartPeripheral,
//         gpio::FunctionI2C,
//         i2c::I2C,
//     },
//     Pins, XOSC_CRYSTAL_FREQ,
// };

use embedded_hal::prelude::_embedded_hal_blocking_i2c_Write as I2cWriter;

// #[repr(u8)]
// pub enum LcdCommands {
    const CLEAR_DISPLAY : u8 = 0x01;
    const RETURN_HOME : u8 = 0x02;
    const ENTRY_MODE_SELECT : u8 = 0x04;
    const DISPLAY_CONTROL : u8 = 0x8;
    const CURSOR_SHIFT : u8 = 0x10;
    const FUNCTION_SET : u8 = 0x20;
    const SET_CGRAM_ADDR : u8 = 0x40;
    const SET_DDRAM_ADDR : u8 = 0x80;
// }

// #[repr(u8)]
// pub enum LcdFunctionParam  {
    const EIGHT_BIT_MODE :u8 = 0x10;
    //FourBitMode = 0x0,
    
    const TWO_LINE_MODE : u8 = 0x08;
    //OneLineMode = 0x00,
    
    const FIVE_BY_TEN_DOTS : u8 = 0x04;
    //FiveByEightDots = 0x0,
// }

// #[repr(u8)]
// pub enum LcdDISPLAY_CONTROLParam {
    const BLINK_ON : u8 = 0x01;
    const CURSOR_ON : u8 = 0x02;
    const DISPLAY_ON : u8 = 0x04;
    const BACKLIGHT_ON : u8 = 0x08;
//}

pub const ENABLE_BIT : u8 = 0x4;
pub const RW_BIT : u8 = 0x2;
pub const DATA_BIT : u8 = 0x1;

pub struct LcdDisplayDriver<I2CDev, E> 
    where
    I2CDev: I2cWriter<Error = E> {
    pub i2c: I2CDev,
    pub delay: cortex_m::delay::Delay,
}

pub trait LcdDisplay//<I2C, E: core::fmt::Debug>
{
    fn init(&mut self);
    fn write4bits(&mut self, value : u8);
    fn send_lcd(&mut self, value : u8, mode : u8);
    fn draw_line(&mut self, line : &str);
    fn draw_lines(&mut self, line1 : &str, line2 : &str);
}

impl<I2CDev, E: core::fmt::Debug> LcdDisplay for LcdDisplayDriver<I2CDev, E> 
    where I2CDev: I2cWriter<Error = E> 
{
    fn write4bits(&mut self, value : u8) 
    {
        self.i2c.write(0x27, &[value]).unwrap();
        self.i2c.write(0x27, &[(value | ENABLE_BIT)]).unwrap();
        self.delay.delay_us(1);
        self.i2c.write(0x27, &[(value | !ENABLE_BIT)]).unwrap();
        self.delay.delay_us(50);
    }

    fn send_lcd(&mut self, value : u8, mode : u8)
    {
        self.write4bits((value & 0xf0) | (mode & 0xf));
        self.write4bits(((value<<4) & 0xf0) | (mode & 0xf));
    }
    
    fn init(&mut self)
    {
        // Give LCD Panel some time to wake up
        self.delay.delay_ms(100);
        
        // Initialize the LCD Panel, pulling Rw and Rs low, BACKLIGHT_ON on
        self.send_lcd(0x08, 0);
        self.delay.delay_ms(1000);
        
        self.write4bits(0x03 << 4);
        self.delay.delay_us(4500);
        self.write4bits(0x03 << 4);
        self.delay.delay_us(4500);
        self.write4bits(0x03 << 4);
        self.delay.delay_us(150);
        
        // Set 4-bit interface
        self.write4bits(0x02 << 4);
        self.delay.delay_us(150);
        
        self.send_lcd(FUNCTION_SET | 
            TWO_LINE_MODE, 0);
        self.delay.delay_us(150);
    
        self.send_lcd(DISPLAY_CONTROL | 
            BLINK_ON |
            CURSOR_ON |
            DISPLAY_ON,
            0);
        self.delay.delay_us(150);
           
        self.send_lcd( 
            ENTRY_MODE_SELECT | 0x02, // EntryModeLeft
            0);
        self.delay.delay_us(150);
    }
    
    fn draw_line(&mut self, line : &str)
    {
        for c in line.bytes() {
            self.send_lcd(
                c as u8,
                DATA_BIT);
            self.delay.delay_us(50);
        }
    }
    
    fn draw_lines(&mut self, line1 : &str, line2 : &str)
    {
        self.send_lcd(RETURN_HOME, 0);
        self.delay.delay_us(2000);
     
        self.send_lcd(
            CLEAR_DISPLAY, 
            0);
        self.delay.delay_us(2000);
    
        self.draw_line(line1);
        
        // Move to line 2
        self.send_lcd(0xc0, 0);
        self.delay.delay_us(2000);
            
        self.draw_line(line2);
    }
}
