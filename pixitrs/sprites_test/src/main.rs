use sfml::window::{Window, Event, Style};
use sfml::graphics::{RenderWindow, Texture, Sprite, Transformable, Drawable, RenderTarget, Color};

use pixitrs::{PixitEngCore};

fn main() {
    
    // Create a new window
    let eng : PixitEngCore = PixitEngCore{
        window: RenderWindow::new((800, 600),
                                "SFML test",
                                Style::CLOSE,
                                &Default::default()),
    };

    let mut window = eng.window;

    // Limit the framerate to 60 frames per second (this step is optional)
    window.set_framerate_limit(60);

    // The main loop - ends as soon as the window is closed
    while window.is_open() {
        // Event processing
        while let Some(event) = window.poll_event() {
            // Request closing for the window
            if event == Event::Closed {
                window.close();
            }
        }

        // Draw everything
        window.clear(Color::rgb(40, 40, 84));

        // Activate the window for OpenGL rendering
        window.set_active(true);

        // End the current frame and display its contents on screen
        window.display();
    }
}
