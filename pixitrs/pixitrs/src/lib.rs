use sfml::window::{Window, Event, Style};
use sfml::graphics::{RenderWindow, Texture, Sprite, Transformable, Drawable, RenderTarget, Color};
use sfml::system::{Vector2f};

pub struct PixitEngCore
{
    pub window : RenderWindow,

}

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
