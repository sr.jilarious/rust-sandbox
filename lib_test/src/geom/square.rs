use crate::geom::Shape;

pub struct Square {
    pub length: f32
}

impl Shape for Square {
    fn area(&self) -> f32 {
        return self.length*self.length;
    }
}
