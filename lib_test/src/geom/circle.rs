// use crate::lib;
use crate::geom::Shape;

pub struct Circle {
    pub radius: f32
}

impl Shape for Circle {
    fn area(&self) -> f32 {
        return std::f32::consts::PI * self.radius * self.radius;
    }
}
