mod geom;

use geom::{Shape, Square, Circle};

fn main() {
    let sq = Square { length: 10.0  };
    let cr = Circle { radius: 10.0  };

    println!("Traits: Circle area: {}, square area: {}", cr.area(), sq.area());
}
