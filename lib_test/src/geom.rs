pub trait Shape { 
    fn area(&self) -> f32;
}

mod circle;
mod square;

pub use circle::Circle as Circle;
pub use square::Square as Square;
