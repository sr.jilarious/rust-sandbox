use std::fs::File;
use std::io::BufRead;
use std::path::Path;

fn main() {
    println!("= Simple file test =");

    let path = Path::new("test_data.txt");
    
    let file = match File::open(&path) {
        Err(why) => 
            panic!("Not able to open file {}: {}", 
                    path.display(), why),
        Ok(file) => file,
    };

    let line_reader = std::io::BufReader::new(file);
    for line in line_reader.lines() {
        if let Ok(l) = line {

            // Skip comment lines.
            if !l.starts_with('#') {
                println!("{}", l);
            }
        }
    }
}
