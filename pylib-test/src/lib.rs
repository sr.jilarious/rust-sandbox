use cpython::{PyResult, Python, py_module_initializer, py_fn};

// add bindings to the generated python module
py_module_initializer!(my_pylib, |py, m| {
    m.add(py, "__doc__", "My fancy python module in Rust!")?;
    m.add(py, "test", py_fn!(py, test_py(name: String)))?;
    Ok(())
});

// logic implemented as a normal rust function
fn test(name: String) -> String {
    format!("Why hello there {}", name).to_string()
}

// rust-cpython aware function. All of our python interface could be
// declared in a separate module.
// Note that the py_fn!() macro automatically converts the arguments from
// Python objects to Rust values; and the Rust return value back into a Python object.
fn test_py(_: Python, name: String) -> PyResult<String> {
    let out = test(name);
    Ok(out)
}
